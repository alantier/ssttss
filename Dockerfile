FROM base/archlinux:latest

RUN pacman -Sy --noconfirm archlinux-keyring
RUN pacman -Syu --noconfirm
RUN pacman-db-upgrade
RUN pacman -S --noconfirm ca-certificates-mozilla
RUN pacman -S --noconfirm make texlive-bin texlive-core texlive-latexextra
RUN pacman -S --noconfirm php git
RUN pacman -S --noconfirm wget && mkdir -p /usr/local/share/texmf && cd /usr/local/share/texmf && wget "http://mirrors.ctan.org/install/fonts/ebgaramond.tds.zip" && bsdtar xf ebgaramond.tds.zip && rm ebgaramond.tds.zip && mktexlsr && updmap-sys --enable Map=EBGaramond.map
RUN rm /var/cache/pacman/pkg/*
