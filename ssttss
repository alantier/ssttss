#!/usr/bin/env php
<?php
/* Copyright 2017 Arsène Lantier <arsene.lantier@laposte.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* XXX: make clean? make distclean? */

$fatal = function(string $fmt, ...$args) use($argv) {
	fprintf(STDERR, "%s: ".$fmt."\n", $argv[0], ...$args);
	die(1);
};

if($argc !== 2) {
	$fatal("usage: %s <build-dir>", $argv[0]);
}

if(!file_exists('ssttss.json')) {
	$fatal("no ssttss.json in cwd");
}

$json = json_decode(file_get_contents('ssttss.json'), true);
if($json === false) $fatal('unreadable or garbage ssttss.json');

if(!isset($json['ssttss']) || $json['ssttss'] !== 1) {
	$fatal('this does not look like a ssttss.json file');
}

$build = realpath($argv[1]);
$src = realpath('.');

if(!file_exists($build) || !is_dir($build) || !is_writeable($build)) {
	$fatal('build directory not a writeable directory');
}

$mf = fopen($build.'/Makefile', 'w'); /* XXX: check errors */

fwrite($mf, "default: all\n");
fwrite($mf, "all: all-pdf all-xhtml all-plain all-epub\n");
fwrite($mf, ".PHONY: default all all-pdf all-xhtml all-plain all-epub\n");

fwrite($mf, "%.html: %.tex mstory.cls\n");
fwrite($mf, "\t./optionize html,final < \$< > \$*-html.tex\n");
fwrite($mf, "\tmake4ht -u \$*-html.tex\n");
fwrite($mf, "\tmv \$*-html.html \$@\n");

foreach([ 'bare', 'neat' ] as $t) {
	fprintf($mf, "%%-%s.xhtml: %%.html\n\t./xhtmlize %s \$< > \$@ || (rm -f \$@; exit 1)\n", $t, $t);
}
fwrite($mf, "%.txt: %.html\n\t./xhtmlize plain \$< > \$@ || (rm -f \$@; exit 1)\n");
fwrite($mf, "%.epub: %-bare.xhtml\n\t./epubize \$@ < \$< || (rm -f \$@; exit 1)\n");

foreach([ 'draft', 'final' ] as $t) {
	foreach([ 'a4', 'a5', 'letter' ] as $p) {
		foreach([ '10pt', '11pt', '12pt' ] as $s) {
			foreach([ 'standard', 'neat', 'luxury' ] as $m) {
				fprintf($mf, "%%-%s-%s-%s-%s.pdf: %%.tex versions.tex mstory.cls\n", $p, $s, $m, $t);
				for($i = 0; $i < 2; ++$i) {
					fprintf(
						$mf,
						"\t./optionize %spaper,%s,%s,%s < \$< | pdflatex -halt-on-error -jobname=\$*-%s-%s-%s-%s || (rm -f \$@; exit 1)\n",
						$p, $s, $m, $t, $p, $s, $m, $t
					);
				}
			}
		}
	}
}

foreach($json['stories'] as $tex) {
	$prefix = pathinfo($tex, PATHINFO_FILENAME);

	fprintf($mf, "%s: %s/%s\n", $tex, $src, $tex); /* XXX: escaping */
	fwrite($mf, "\tcp -a \$< \$@\n");

	foreach([ 'draft', 'final' ] as $t) {
		foreach(['a4', 'a5', 'letter' ] as $p) {
			foreach([ '10pt', '11pt', '12pt' ] as $s) {
				foreach([ 'standard', 'neat', 'luxury' ] as $m) {
					fprintf($mf, "all-pdf: %s-%s-%s-%s-%s.pdf\n", $prefix, $p, $s, $m, $t);
				}
			}
		}
	}

	foreach([ 'bare', 'neat' ] as $t) {
		fprintf($mf, "all-xhtml: %s-%s.xhtml\n", $prefix, $t);
	}
	fprintf($mf, "all-plain: %s.txt\n", $prefix);
	fprintf($mf, "all-epub: %s.epub\n", $prefix);
}

fwrite($mf, "versions.tex: ".implode(' ', $json['stories'])."\n"); /* XXX: deps escaping */
fwrite($mf, "\techo '\\newcommand{\ssttssgitver}{'"
	   .escapeshellarg(trim(shell_exec('cd '.escapeshellarg(__DIR__).'; git describe --always --abbrev=7')))
	   ."'}' > \$@\n");
fwrite($mf, "\techo '\\newcommand{\gitdate}{'`git show -s --date=\"format:%B %d, %Y\" --format=\"%ad\"`'}' >> \$@\n");
fwrite($mf, "\techo '\\newcommand{\gitver}{'`git describe --always --abbrev=7`'}' >> \$@\n");

fclose($mf);

foreach([ 'mstory.cls', 'optionize', 'xhtmlize', 'neat.css', 'epubize' ] as $f) {
	shell_exec('cp '.escapeshellarg(__DIR__.'/'.$f).' '.escapeshellarg($build)); /* XXX: check errors */
}
