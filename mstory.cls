\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{mstory}
\RequirePackage{etoolbox}

\DeclareOption{standard}{
  \AtEndOfClass{
    \setlrmarginsandblock{.1\paperwidth}{.15\paperwidth}{*}
    \setulmarginsandblock{.125\paperwidth}{.175\paperwidth}{*}
  }
}
\DeclareOption{neat}{
  \AtEndOfClass{
    \setlrmarginsandblock{.1333333\paperwidth}{.2\paperwidth}{*}
    \setulmarginsandblock{.1666666\paperwidth}{.2333333\paperwidth}{*}
  }
}
\DeclareOption{luxury}{
  \AtEndOfClass{
    \setlrmarginsandblock{.15\paperwidth}{.225\paperwidth}{*}
    \setulmarginsandblock{.1875\paperwidth}{.2625\paperwidth}{*}
  }
}

\newbool{a5}
\boolfalse{a5}
\newbool{twocol}
\booltrue{twocol}

\DeclareOption{twocolumn}{\booltrue{twocol}}
\DeclareOption{onecolumn}{\boolfalse{twocol}}

\DeclareOption{a5paper}{
  \booltrue{a5}
  \PassOptionsToClass{a5paper}{memoir}
}

\newbool{html}
\boolfalse{html}
\DeclareOption{html}{\booltrue{html}}
\DeclareOption{nohtml}{\boolfalse{html}}

\DeclareOption*{
  \PassOptionsToClass{\CurrentOption}{memoir}
}

\ProcessOptions*

\ifbool{twocol}{
  \ifbool{a5}{}{
    \PassOptionsToClass{twocolumn,article}{memoir}
  }
}{}

\AtEndOfClass{
  \setheadfoot{15pt}{25pt}
  \setheaderspaces{*}{10pt}{*}
  \checkandfixthelayout
  \sloppybottom
}

\LoadClass{memoir}

\ifbool{html}{
  % hack to add books to generated TOC
  \renewcommand{\book}[1]{\stepcounter{book}\part*{Book \thebook \\ #1}\addcontentsline{toc}{part}{#1}}
}{
  % htlatex doesn't like ligatures and other fancy symbols
  \usepackage[final,tracking=true,spacing=true]{microtype}
  \usepackage{ebgaramond}
}

\aliaspagestyle{title}{empty}
\aliaspagestyle{chapter}{empty}
\makepsmarks{myheadings}{%
  \nouppercaseheads
  \createmark{chapter}{both}{nonumber}{}{}
}
\pagestyle{myheadings}
\makeoddhead{myheadings}{}{\textit{\rightmark}}{\thepage}
\makeevenhead{myheadings}{\thepage}{\textit{\leftmark}}{}
\renewcommand*{\thechapter}{\Roman{chapter}}

\newcommand{\sectionbreak}{\begin{center} \raisebox{-1.25ex}{*}\hspace{-.15em}\raisebox{-.5ex}{*}\hspace{-.15em}\raisebox{-1.25ex}{*} \end{center}}

\input{versions.tex}
