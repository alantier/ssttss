ssttss
======

A Simple STory TypeSetter. Released under the Apache License, version 2.0.

Outputs
=======

* many PDF files (see table below)
* one EPUB file
* two XHTML files (with and without fancy styling)
* one plain text file

Usage
=====

* Write `ssttss.json` in the same directory as your stories (`.tex` files)
* `mkdir build && ssttss build`
* `cd build; make`

Dependencies
============

* PHP
* `pdflatex`
* `ebgaramond` CTAN package

`ssttss.json` syntax
====================

~~~
{
	"ssttss": 1,
	"stories": [ "foo.tex", "bar.tex", "baz.tex" ]
}
~~~

Demonstration
=============

See the `demo` directory for sources.

Compiled outputs ([there are much more ; see here for a full list of generated files](https://alantier.gitlab.io/ssttss/index.xhtml)):

* PDF, best typography, ideal for print:

| Paper size   | Standard (for saving paper)          | Neat (recommended)                   | Luxurious (greater reading comfort)  |
| ------------ | ------------------------------------ | ------------------------------------ | ------------------------------------ |
| A5 paper     | [10pt][01] ; [11pt][02] ; [12pt][03] | [10pt][04] ; [11pt][05] ; [12pt][06] | [10pt][07] ; [11pt][08] ; [12pt][09] |
| A4 paper     | [10pt][11] ; [11pt][12] ; [12pt][13] | [10pt][14] ; [11pt][15] ; [12pt][16] | [10pt][17] ; [11pt][18] ; [12pt][19] |
| Letter paper | [10pt][21] ; [11pt][22] ; [12pt][23] | [10pt][24] ; [11pt][25] ; [12pt][26] | [10pt][27] ; [11pt][28] ; [12pt][29] |

Use something like [`pdfimpose`](https://gitlab.com/alantier/pdfimpose/) to generate booklets or signatures for binding.

* [EPUB](https://alantier.gitlab.io/ssttss/demo.epub), for e-readers;

* [Bare XHTML](https://alantier.gitlab.io/ssttss/demo-bare.xhtml), for further conversion; [Neat XHTML](https://alantier.gitlab.io/ssttss/demo-neat.xhtml), for reading in a browser;

* [Plain text](https://alantier.gitlab.io/ssttss/demo.txt), for further conversion.

[01]: https://alantier.gitlab.io/ssttss/demo-a5-10pt-standard-final.pdf
[02]: https://alantier.gitlab.io/ssttss/demo-a5-11pt-standard-final.pdf
[03]: https://alantier.gitlab.io/ssttss/demo-a5-12pt-standard-final.pdf
[04]: https://alantier.gitlab.io/ssttss/demo-a5-10pt-neat-final.pdf
[05]: https://alantier.gitlab.io/ssttss/demo-a5-11pt-neat-final.pdf
[06]: https://alantier.gitlab.io/ssttss/demo-a5-12pt-neat-final.pdf
[07]: https://alantier.gitlab.io/ssttss/demo-a5-10pt-luxury-final.pdf
[08]: https://alantier.gitlab.io/ssttss/demo-a5-11pt-luxury-final.pdf
[09]: https://alantier.gitlab.io/ssttss/demo-a5-12pt-luxury-final.pdf
[11]: https://alantier.gitlab.io/ssttss/demo-a4-10pt-standard-final.pdf
[12]: https://alantier.gitlab.io/ssttss/demo-a4-11pt-standard-final.pdf
[13]: https://alantier.gitlab.io/ssttss/demo-a4-12pt-standard-final.pdf
[14]: https://alantier.gitlab.io/ssttss/demo-a4-10pt-neat-final.pdf
[15]: https://alantier.gitlab.io/ssttss/demo-a4-11pt-neat-final.pdf
[16]: https://alantier.gitlab.io/ssttss/demo-a4-12pt-neat-final.pdf
[17]: https://alantier.gitlab.io/ssttss/demo-a4-10pt-luxury-final.pdf
[18]: https://alantier.gitlab.io/ssttss/demo-a4-11pt-luxury-final.pdf
[19]: https://alantier.gitlab.io/ssttss/demo-a4-12pt-luxury-final.pdf
[21]: https://alantier.gitlab.io/ssttss/demo-letter-10pt-standard-final.pdf
[22]: https://alantier.gitlab.io/ssttss/demo-letter-11pt-standard-final.pdf
[23]: https://alantier.gitlab.io/ssttss/demo-letter-12pt-standard-final.pdf
[24]: https://alantier.gitlab.io/ssttss/demo-letter-10pt-neat-final.pdf
[25]: https://alantier.gitlab.io/ssttss/demo-letter-11pt-neat-final.pdf
[26]: https://alantier.gitlab.io/ssttss/demo-letter-12pt-neat-final.pdf
[27]: https://alantier.gitlab.io/ssttss/demo-letter-10pt-luxury-final.pdf
[28]: https://alantier.gitlab.io/ssttss/demo-letter-11pt-luxury-final.pdf
[29]: https://alantier.gitlab.io/ssttss/demo-letter-12pt-luxury-final.pdf
